from aiogram import types, F, Router
from aiogram.types import Message, ReplyKeyboardMarkup
from aiogram.utils.keyboard import ReplyKeyboardBuilder
from forms import RegistrationForm, GetKilled
from aiogram.fsm.context import FSMContext
from aiogram.types import FSInputFile
import requests
from transliterate import translit
import aiogram.utils.markdown as md
from forms import GetKilled, RegistrationForm
import pyqrcode
from admin import start_game,restart_game,stop_game,get_alive_players,broadcast
from urllib.parse import unquote
import aiohttp


context = None

builder = ReplyKeyboardBuilder()
builder.row(
    types.KeyboardButton(text='Профиль'),
    (types.KeyboardButton(text='Контракт'))
)
builder.row(types.KeyboardButton(text='Ликвидировать'))


api_url = "http://172.28.0.3:5000/"

def backward_translate(data:str):
    return str(translit(data,language_code='ru')).upper()

def fix(data:str):
    if data.find("/u")!=-1:
        return unqoute(data)
    return backward_translate(data)
async def start_handler(msg: Message,state:FSMContext):
    context = requests.get(api_url+f"players/{msg.from_user.id}/")
    uname = None
    if context.status_code==404:
        await msg.answer("Привет! Введи свои настоящие имя и фамилию:")
        await state.set_state(RegistrationForm.name)
    else:
        uname = backward_translate(context.json()['username'])
        print(context)
        await msg.answer(f"С возвращением, {md.bold(uname)}",reply_markup=builder.as_markup(resize_keyboard=True))
    print(uname) 


async def get_name(msg: Message,state:FSMContext):
    await msg.answer(f"Правление кланов принимает тебя, {msg.text}",reply_markup=builder.as_markup(resize_keyboard=True))
    await state.update_data(name=msg.text)
    async with aiohttp.ClientSession() as session:
        async with session.get(api_url+"players/add/",params={'username':msg.text,"tgid":msg.from_user.id}) as response:
            print(response.status)
    await state.clear()

async def message_handler(msg:Message,state:FSMContext):
    if msg.text.lower()== "профиль":
        await get_profile(msg)
    elif msg.text.lower() == "контракт":
        await get_contract(msg)
    elif msg.text.lower() == "ликвидировать":
        await kill_player(msg,state)
    elif msg.text.lower() == "/game_start":
        await start_game(msg)
    elif msg.text.lower()=="/game_stop":
        await stop_game(msg)
    elif msg.text.lower()=='/game_restart':
        await restart_game(msg)
    elif msg.text.lower()=="/get_alives":
        await get_alive_players(msg)
    elif msg.text.lower().find("/broadcast")==0:
        await broadcast(msg,msg.text.lower().split("/broadcast")[1])


async def kill_player(msg:Message,state:FSMContext):
    async with aiohttp.ClientSession() as session:
        async with session.get(api_url+f"players/get_contract/{msg.from_user.id}/") as r:
            if r.status == 404 or r.status == 500:
                await msg.answer("Случилась какая-то ошибка. Сообщите админам",reply_markup=builder.as_markup(resize_keyboard=True))
            else:
                data = await r.json()
                if data['alive'] == False:
                    await msg.answer("💀 Вы ликвидированы 💀",reply_markup=builder.as_markup(resize_keyboard=True))
                else:
                    if data['target'] == 'none':
                        await msg.answer("⚠ Сражение еще не началось",reply_markup=builder.as_markup(resize_keyboard=True))
                    else:
                        await msg.answer("Введите код для ликвидации противника:",reply_markup=builder.as_markup(resize_keyboard=True))
                        await state.set_state(GetKilled.player_id)


async def try_kill(msg:Message,state:FSMContext):
    print("Trying to kill")
    async with aiohttp.ClientSession() as session:
        async with session.get(api_url+f"game/kill/{msg.from_user.id}/{msg.text}") as r:
            if r.status==404 or r.status==500:
                await msg.answer("⛔ Ты не можешь ликвидировать этого игрока!!! ⛔",reply_markup=builder.as_markup(resize_keyboard=True))
                await state.clear()
            else:
                await state.update_data(player_id = msg.text)
                await msg.answer("✅ Отличная работа, игрок!!!\n Проверь свой контракт - там новая цель",reply_markup=builder.as_markup(resize_keyboard=True))
                await state.clear()
                await msg.bot.send_message(await r.text,"🔴 Вас ликвидировали 🔴")        



async def get_profile(msg:Message):
    async with aiohttp.ClientSession() as session:
        async with session.get(api_url+f"players/{msg.from_user.id}/") as r:
            if r.status== 404 or r.status == 500:
                await msg.answer("Случилась какая-то ошибка. Сообщите админам",reply_markup=builder.as_markup(resize_keyboard=True)   )
            else:
                data = await r.json()
                player_uid = str(data['uuid'])
                qr_code = pyqrcode.create(player_uid)
                fname = str(msg.from_user.id)+".png"
                qr_code.png("qrcodes/"+fname,scale=6)

                response = "Данные об игроке: \n\n"
                response += "🏷️ Имя:  " + md.bold(fix(data['username'])) + '\n\n'
                response += "💥 Ликвидировано:  " + str(data['kills']) + '\n\n'
                response += '⚠ Особо опасный боец: ' + ("Да" if data['excomunnicado'] else "Нет") + "\n\n"
                response += '🌐 Ваш код (такой же и в QR): ' + '`'+str(data['uuid']) + '`\n\n'
                if data['isAlive']:
                    response += '🟢 Статус: в игре\n'
                else:
                    response += '🔴 Статус: ликвидирован'
                await msg.answer_photo(FSInputFile("qrcodes/"+fname),caption=response)

async def get_contract(msg:Message):
    async with aiohttp.ClientSession() as session:
        async with session.get(api_url+f"players/get_contract/{msg.from_user.id}/") as r:
            if r.status== 404 or r.status == 500:
                await msg.answer("Случилась какая-то ошибка. Сообщите админам")
            else:
                data = await r.json()
                print(data)
                if data['alive'] == False:
                    await msg.answer("🔴 Вы ликвидированы, у вас нет контрактов")
                else:
                    if data['target'] == "none":
                        await msg.answer("⚠ Сражение еще не началось")
                    else:
                        await msg.answer(f"🎯 Твоя цель: {md.bold(fix(data['target']))}")
    