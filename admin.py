from aiogram import types, F, Router
from aiogram.types import Message, ReplyKeyboardMarkup
from forms import RegistrationForm, GetKilled
from aiogram.fsm.context import FSMContext
from aiogram.types import FSInputFile
import requests
from transliterate import translit
import aiogram.utils.markdown as md
from forms import GetKilled, RegistrationForm
import pyqrcode
from urllib.parse import unquote

api_url = "http://172.28.0.3:5000/"

def backward_translate(data:str):
    return str(translit(data,language_code='ru')).upper()
def fix(data:str):
    if data.find("/u")!=-1:
        return unqoute(data)
    return backward_translate(data)
async def start_game(msg:Message):
    if msg.from_user.id!=1035612511:
        return
    r = requests.get(api_url+"game/start")
    if r.status_code==200:
        pass
        await msg.answer("[INFO] Игра запущена")
        await broadcast(msg,"⚔ Да начнется сражение ⚔")

async def stop_game(msg:Message):
    if msg.from_user.id!=1035612511:
        return
    r = requests.get(api_url+"game/stop")
    if r.status_code==200:
        pass
        await msg.answer("[INFO] Игра окончена")
        await broadcast(msg,"🏁 Сражение окончено. Ожидайте дальнейших указаний 🏁")

async def restart_game(msg:Message):
    if msg.from_user.id!=1035612511:
        return
    r = requests.get(api_url+"game/restart")
    if r.status_code==200:
        await msg.answer("[INFO] Игра окончена")
        await broadcast(msg,"Все ликвидированные игроки вновь в игре. Проверьте свои контракты")

async def get_alive_players(msg:Message):
    if msg.from_user.id!=1035612511:
        return
    r = requests.get(api_url+"players/alive/")
    if r.status_code==200:
        response = "Живых игроков:\n\n"
        print(r.text)
        for player in r.json():
            response += fix(player)+"\n"
        response += "\nВсего живых: " + str(len(r.json()))
        await msg.answer(response)
async def broadcast(msg:Message,text:str):
    users = requests.get(api_url+"players/getAllTG/").json()
    for user in users:
        await msg.bot.send_message(int(user),text)
    